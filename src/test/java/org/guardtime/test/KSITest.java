package org.guardtime.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.hashing.HashAlgorithm;
import org.junit.Test;

public class KSITest extends TestsBase {

  @Test
  public void shouldCreateDataHash() {
    DataHash dataHash = KSI.calculateHash(HashAlgorithm.SHA2_256, "new.zip");
    assertEquals(HashAlgorithm.SHA2_256, dataHash.getAlgorithm());
    assertNotNull(dataHash.getValue());
  }

}
