package org.guardtime.test;

import static org.apache.commons.io.FileUtils.copyFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class TestsBase {


  File createFileWithPrefix(int i) {
    try {
      File source = singmeFile();
      File destination = File.createTempFile(i + "test", ".txt");
      copyFile(source, destination);
      return destination;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  List<File> thousandRandomFiles() {
    return createFiles(1000);
  }

  List<File> fiveHundredRandomFiles() {
    return createFiles(500);
  }

  List<File> tenThousandRandomFiles() {
    return createFiles(10000);
  }

  File singmeFile() {
    return new File("signme.txt");
  }

  private List<File> createFiles(int i1) {
    var files = new ArrayList<File>();
    for (int i = 0; i < i1; i++) {
      files.add(createFileWithPrefix(i));
    }
    return files;
  }
}
