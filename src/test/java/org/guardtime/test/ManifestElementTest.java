package org.guardtime.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.guardtime.ksi.hashing.HashAlgorithm;
import org.junit.Test;

public class ManifestElementTest extends TestsBase {

  @Test
  public void shouldCreateDataHash() {
    var manifestElement = new ManifestElement(HashAlgorithm.SHA2_256, singmeFile());
    assertEquals("signme.txt", manifestElement.uri());
    assertEquals(HashAlgorithm.SHA2_256, manifestElement.hashAlgorithm());
    assertNotNull(manifestElement.hash().getValue());
    assertTrue(manifestElement.getContentLength() > 0);
  }
}
