package org.guardtime.test;

import static org.junit.Assert.assertEquals;

import com.guardtime.ksi.hashing.HashAlgorithm;
import java.io.File;
import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ContainerTest extends TestsBase {

  @Before
  public void setUp() {
    new File("testwrite.zip").delete();
  }

  @After
  public void tearDown() {
    new File("testwrite.zip").delete();
  }

  @Test
  public void shouldWriteAndSignFileReadItAndDeleteIt() {
    new Container("testwrite.zip").signFile(HashAlgorithm.SHA2_256, "test.txt");

    var readedOnceAgainContainer = new Container("testwrite.zip");
    assertEquals("test.txt", readedOnceAgainContainer.signedFileUris().get(0));

    var manifest = readedOnceAgainContainer.signedManifests().get(0);

    assertEquals("test.txt", manifest.signedFilesUris().get(0));
    assertEquals("/META-INF/manifest1.tlv", manifest.uri());
    assertEquals("/META-INF/signature1.ksi", manifest.signatureUri());

    var manifestElement = manifest.datafiles().get(0);
    assertManifestName("test.txt", manifestElement);

    var actualHash = KSI.calculateHash(HashAlgorithm.SHA2_256, "test.txt").toString();
    assertEquals("SHA-256:[9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08]",
        actualHash);
    assertEquals(manifestElement.hash().toString(), actualHash);

    Container containerWithRemovedSignatures = new Container("testwrite.zip")
        .removeSignature("/META-INF/signature1.ksi");
    assertEquals(0, containerWithRemovedSignatures.signedFileUris().size());
    assertEquals(0, containerWithRemovedSignatures.signedManifests().size());
  }

  @Test
  public void shouldWriteMultipleFilesAndTestIfTheyWereWrittenAndTestIfManifestAndSignatureWereCreated() {
    new Container("testwrite.zip")
        .signFilesWithFileUris(HashAlgorithm.SHA2_256, Arrays.asList("test.txt", "signme.txt"));

    Container readedOnceAgainContainer = new Container("testwrite.zip");

    var signedFiles = readedOnceAgainContainer.signedFileUris();
    assertEquals("test.txt", signedFiles.get(0));
    assertEquals("signme.txt", signedFiles.get(1));

    var manifests = readedOnceAgainContainer.signedManifests();
    assertEquals(1, manifests.size());

    var manifest = manifests.get(0);
    assertEquals("test.txt", manifest.signedFilesUris().get(0));
    assertEquals("signme.txt", manifest.signedFilesUris().get(1));
    assertEquals("/META-INF/manifest1.tlv", manifest.uri());
    assertEquals("/META-INF/signature1.ksi", manifest.signatureUri());

    assertManifestName("test.txt", manifest.datafiles().get(0));
    assertManifestName("signme.txt", manifest.datafiles().get(1));
  }

  @Test
  public void shouldWrite1000FilesAndTestThat2ManfiestWereCreatedBecauseSizeOfOneIsMax64Kb() {
    Container container = new Container("testwrite.zip");
    container.signFiles(HashAlgorithm.SHA2_256, thousandRandomFiles());

    Container readedOnceAgainContainer = new Container("testwrite.zip");
    var manifests = readedOnceAgainContainer.signedManifests();
    assertEquals(2, manifests.size());

    int size1 = manifests.get(0).signedFilesUris().size();
    int size2 = manifests.get(1).signedFilesUris().size();
    assertEquals(1000, size1 + size2);

    assertEquals("/META-INF/manifest1.tlv", manifests.get(0).uri());
    assertEquals("/META-INF/signature1.ksi", manifests.get(0).signatureUri());

    assertEquals("/META-INF/manifest2.tlv", manifests.get(1).uri());
    assertEquals("/META-INF/signature2.ksi", manifests.get(1).signatureUri());

    readedOnceAgainContainer.removeSignature("/META-INF/signature1.ksi");
    var manifestsAfterRemoval = readedOnceAgainContainer.signedManifests();
    assertEquals(1, manifestsAfterRemoval.size());

    assertEquals(1000 - size1, manifestsAfterRemoval.get(0).signedFilesUris().size());
  }

  @Test(timeout = 50000)
  public void shouldTestCan10000FilesBeWrittenQuickly() {
    Container container = new Container("testwrite.zip");
    container.signFiles(HashAlgorithm.SHA2_256, tenThousandRandomFiles());

    Container readedOnceAgainContainer = new Container("testwrite.zip");
    var manifests = readedOnceAgainContainer.signedManifests();
    assertEquals(13, manifests.size());

    assertEquals(10000, manifests.stream().flatMap(it -> it.signedFilesUris().stream()).count());
  }

  private void assertManifestName(String manifestUri, ManifestElement manifestElement) {
    assertEquals(manifestUri, manifestElement.uri());
    assertEquals(HashAlgorithm.SHA2_256, manifestElement.hashAlgorithm());
    assertEquals(HashAlgorithm.SHA2_256, manifestElement.hash().getAlgorithm());
  }


}


