package org.guardtime.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;

public class ZipContainerTest extends TestsBase {

  @Before
  public void setUp() {
    new File("new.zip").delete();
  }

  @Test
  public void shouldAddSimpleFile() {
    new ZipContainer("new.zip").add(singmeFile());
    assertUnpackedFileCorrect();
  }

  @Test
  public void shouldAdd500RandomFiles() {
    ZipContainer zipContainer = new ZipContainer("new.zip");
    zipContainer.add(fiveHundredRandomFiles());
    assertEquals(500, new ZipContainer("new.zip").fileHeaders().size());
  }

  @Test
  public void shouldAdd10000RandomFiles() {
    ZipContainer zipContainer = new ZipContainer("new.zip");
    zipContainer.add(tenThousandRandomFiles());
    assertEquals(10000, new ZipContainer("new.zip").fileHeaders().size());
  }

  @Test
  public void shouldAddOne1GBFile() throws Exception {
    new ZipContainer("new.zip").add(veryBigFile());
    assertEquals(1, new ZipContainer("new.zip").fileHeaders().size());
  }

  @Test
  public void shouldAddInputStream() throws Exception {
    new ZipContainer("new.zip").add("signme.txt", new FileInputStream(singmeFile()));
    assertUnpackedFileCorrect();
  }

  private File veryBigFile() throws IOException {
    var file2 = File.createTempFile("large_", ".tmp");
    RandomAccessFile f2 = new RandomAccessFile(file2, "rw");
    f2.setLength(1024 * 1024 * 1024);
    f2.close();
    return file2;
  }

  private void assertUnpackedFileCorrect() {
    var filenames = new ArrayList<>();
    var contents = new ArrayList<>();

    new ZipContainer("new.zip").readFileByFile((bytes, filename) -> {
      filenames.add(filename);
      contents.add(bytes);
    });

    assertEquals(filenames.size(), 1);
    assertEquals(contents.size(), 1);

    assertEquals(filenames.get(0), "signme.txt");
    assertEquals(new String((byte[]) contents.get(0), StandardCharsets.UTF_8), "Sign Me!\n");
  }


}
