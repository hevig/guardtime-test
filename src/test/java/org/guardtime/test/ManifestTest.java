package org.guardtime.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class ManifestTest extends TestsBase {

  @Test
  public void shouldCreateDataHash() {
    var manifest = new Manifest(2);
    assertNull(manifest.uri());
    assertEquals("/META-INF/signature2.ksi", manifest.signatureUri());
    assertEquals(0, manifest.signedFilesUris().size());
  }

}
