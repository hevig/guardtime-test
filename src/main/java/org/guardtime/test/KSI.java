package org.guardtime.test;

import com.guardtime.ksi.Signer;
import com.guardtime.ksi.SignerBuilder;
import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.hashing.DataHasher;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.service.KSISigningClientServiceAdapter;
import com.guardtime.ksi.service.client.KSIServiceCredentials;
import com.guardtime.ksi.service.client.ServiceCredentials;
import com.guardtime.ksi.service.client.http.CredentialsAwareHttpSettings;
import com.guardtime.ksi.service.http.simple.SimpleHttpSigningClient;
import com.guardtime.ksi.unisignature.KSISignature;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.FileUtils;

public class KSI {


  private static final String USER = "ot.sr7AW2";
  private static final String PASS = "trFam0cEb6MH";
  private static final String URL = "http://tryout.guardtime.net:8080/gt-signingservice";

  static DataHash calculateHash(HashAlgorithm hashAlgorithm, String path) {
    return calculateHash(hashAlgorithm, new File(path));
  }

  static DataHash calculateHash(HashAlgorithm hashAlgorithm, File containerFile) {
    try {
      return calculateHash(hashAlgorithm,
          new ByteArrayInputStream(FileUtils.readFileToByteArray(containerFile)));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  static byte[] hash(HashAlgorithm hashAlgorithm, byte[] bytes) {
    return hash(hashAlgorithm, new ByteArrayInputStream(bytes));
  }

  private static DataHash calculateHash(HashAlgorithm hashAlgorithm, InputStream inputStream) {
    DataHasher dataHasher = new DataHasher(hashAlgorithm);
    dataHasher = dataHasher.addData(inputStream);
    return dataHasher.getHash();
  }

  private Signer signer() {
    ServiceCredentials credentials = new KSIServiceCredentials(USER, PASS);
    CredentialsAwareHttpSettings settings = new CredentialsAwareHttpSettings(URL, credentials);
    SimpleHttpSigningClient signingClient = new SimpleHttpSigningClient(settings);
    return new SignerBuilder()
        .setSigningService(new KSISigningClientServiceAdapter(signingClient))
        .build();
  }

  private static byte[] signFile(DataHash manifestDataHash) throws KSIException {
    Signer signer = new KSI().signer();
    KSISignature signature = signer.sign(manifestDataHash);
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    signature.writeTo(byteArrayOutputStream);
    return byteArrayOutputStream.toByteArray();
  }

  private static byte[] hash(HashAlgorithm hashAlgorithm, ByteArrayInputStream isFromManifest) {
    try {
      return signFile(calculateHash(hashAlgorithm, isFromManifest));
    } catch (KSIException e) {
      throw new RuntimeException(e);
    }
  }
}
