package org.guardtime.test;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;

public class ZipContainer {

  ZipFile zipFile;

  ZipContainer(String path) {
    try {
      zipFile = new ZipFile(path);
    } catch (ZipException e) {
      e.printStackTrace();
    }
  }

  List<FileHeader> fileHeaders() {
    try {
      if (!isEmpty()) {
        return new ArrayList<>();
      }
      return zipFile.getFileHeaders();
    } catch (ZipException e) {
      throw new RuntimeException(e);
    }
  }

  ZipContainer(File inputFile) {
    try {
      zipFile = new ZipFile(inputFile);
    } catch (ZipException e) {
      e.printStackTrace();
    }
  }

  ZipContainer readFileByFile(BiConsumer<byte[], String> function) {
    try {
      fileHeaders().forEach(fileHeader -> {
        try {
          ZipInputStream inputStream = zipFile.getInputStream(fileHeader);
          function.accept(inputStream.readAllBytes(), fileHeader.getFileName());
        } catch (ZipException | IOException e) {
          e.printStackTrace();
        }
      });
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return this;
  }

  ZipContainer add(List<File> files) {
    try {
      zipFile.addFiles(new ArrayList(files), new ZipParameters());
    } catch (ZipException e) {
      throw new RuntimeException(e);
    }
    return this;
  }

  ZipContainer add(File file) {
    try {
      ArrayList sourceFileList = new ArrayList();
      sourceFileList.add(file);
      zipFile.addFiles(sourceFileList, new ZipParameters());
    } catch (ZipException e) {
      throw new RuntimeException(e);
    }
    return this;
  }

  ZipContainer add(String filepath, byte[] bytes) {
    return add(filepath, new ByteArrayInputStream(bytes));
  }

  ZipContainer add(String filepath, InputStream inputStream) {
    try {
      ZipParameters parameters = new ZipParameters();
      parameters.setSourceExternalStream(true);
      parameters.setFileNameInZip(filepath);
      zipFile.addStream(inputStream, parameters);
    } catch (ZipException e) {
      throw new RuntimeException(e);
    }
    return this;
  }


  ZipContainer remove(String file) {
    try {
      zipFile.removeFile(file);
    } catch (ZipException e) {
      throw new RuntimeException(e);
    }
    return this;
  }

  ZipContainer removeAll(List<String> files) {
    files.forEach(s -> {
      try {
        zipFile.removeFile(s);
      } catch (ZipException e) {
        throw new RuntimeException(e);
      }
    });
    return this;
  }

  private boolean isEmpty() {
    return zipFile.isValidZipFile();
  }
}
