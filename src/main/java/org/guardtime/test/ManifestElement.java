package org.guardtime.test;

import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVElement;
import com.guardtime.ksi.tlv.TLVParserException;
import com.guardtime.ksi.tlv.TLVStructure;
import java.io.File;

public class ManifestElement extends TLVStructure {

  private String uri;
  private DataHash hash;

  private static final int DATAFILE_TYPE = 0x1;
  private static final int DATAFILE_URI_ELEMENT_TYPE = 0x2;
  private static final int DATAFILE_HASH_ALGORITHM_ELEMENT_TYPE = 0x3;
  private static final int DATAFILE_HASH_TYPE = 0x4;

  int getContentLength() {
    return rootElement.getContentLength();
  }

  ManifestElement(TLVElement rootElement) throws TLVParserException {
    super(rootElement);
    for (TLVElement element : rootElement.getChildElements()) {
      TLVElement tlvElement = readOnce(element);
      switch (element.getType()) {
        case DATAFILE_URI_ELEMENT_TYPE:
          this.uri = tlvElement.getDecodedString();
          break;
        case DATAFILE_HASH_ALGORITHM_ELEMENT_TYPE:
          // Information about manifestHash is already in Datahash object
          // use HashAlgorithm.getByName(this.readOnce(element).getDecodedString()); to decode
          break;
        case DATAFILE_HASH_TYPE:
          this.hash = tlvElement.getDecodedDataHash();
          break;
      }
    }
  }

  ManifestElement(HashAlgorithm hashAlgorithm, File containerFile) {
    add(containerFile.getName(), hashAlgorithm, KSI.calculateHash(hashAlgorithm, containerFile));
  }

  String uri() {
    return uri;
  }

  DataHash hash() {
    return hash;
  }

  HashAlgorithm hashAlgorithm() {
    return hash.getAlgorithm();
  }

  @Override
  public int getElementType() {
    return 1;
  }

  private void add(String uri, HashAlgorithm hashAlgorithm, DataHash hash) {
    this.uri = uri;
    this.hash = hash;

    rootElement = new TLVElement(false, false, DATAFILE_TYPE);
    addElementToRoot(DATAFILE_HASH_ALGORITHM_ELEMENT_TYPE, hashAlgorithm.getName());
    addElementToRoot(TLVElement(hash));
    addElementToRoot(DATAFILE_URI_ELEMENT_TYPE, uri);
  }

  private void addElementToRoot(TLVElement tlvElement) {
    try {
      rootElement.addChildElement(tlvElement);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void addElementToRoot(int datafileHashAlgorithmType, String hashname) {
    try {
      addElementToRoot(TLVElement(datafileHashAlgorithmType, hashname));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private TLVElement TLVElement(DataHash hash) {
    try {
      return TLVElement.create(DATAFILE_HASH_TYPE, hash);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private TLVElement TLVElement(int datafileHashAlgorithmType, String name) {
    try {
      return TLVElement.create(datafileHashAlgorithmType, name);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
