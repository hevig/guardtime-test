package org.guardtime.test;

import com.guardtime.ksi.tlv.TLVElement;
import com.guardtime.ksi.tlv.TLVParserException;
import com.guardtime.ksi.tlv.TLVStructure;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Manifest extends TLVStructure {

  private static final int MANIFEST = 0x0;
  private static final int DATAFILE = 0x1;
  private static final int SIGNATURE_URI = 0x10;

  private List<ManifestElement> datafiles = new ArrayList<>();
  private String signatureUri;
  private String uri;

  Manifest(String uri, byte[] contents) throws TLVParserException {
    this(Manifest.createTLVElementFrom(contents));
    this.uri = uri;
  }

  private Manifest(TLVElement rootElement) throws TLVParserException {
    super(rootElement);
    for (TLVElement tlvElement : rootElement.getChildElements()) {
      switch (tlvElement.getType()) {
        case (SIGNATURE_URI):
          signatureUri = readOnce(tlvElement).getDecodedString();
          break;
        case (DATAFILE):
          datafiles.add(new ManifestElement(tlvElement));
          break;
      }
    }
  }

  Manifest(Integer newSignatureId) {
    rootElement = new TLVElement(false, false, MANIFEST);
    add("/META-INF/signature" + newSignatureId + ".ksi");
  }

  Manifest add(ManifestElement manifestElement) {
    datafiles.add(manifestElement);
    try {
      rootElement.addChildElement(manifestElement.getRootElement());
    } catch (TLVParserException e) {
      e.printStackTrace();
    }
    return this;
  }

  List<String> signedFilesUris() {
    return datafiles().stream().map(ManifestElement::uri).collect(Collectors.toList());
  }

  List<ManifestElement> datafiles() {
    return datafiles;
  }

  String signatureUri() {
    return signatureUri;
  }

  String uri() {
    return uri;
  }

  int getContentLength() {
    return rootElement.getContentLength();
  }

  @Override
  public int getElementType() {
    return 0;
  }

  @Override
  public String toString() {
    return rootElement.toString();
  }

  private void add(String signatureUri) {
    this.signatureUri = signatureUri;
    try {
      rootElement.addChildElement(create(signatureUri));
    } catch (Exception e) {
      throw new RuntimeException();
    }
  }

  private static TLVElement create(String signatureUri) {
    try {
      return TLVElement.create(SIGNATURE_URI, signatureUri);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  private static TLVElement createTLVElementFrom(byte[] contents) {
    try {
      return TLVElement.create(contents);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
