package org.guardtime.test;

import static java.util.Arrays.asList;

import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVElement;
import com.guardtime.ksi.tlv.TLVParserException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Container {

  private static final HashAlgorithm MANIFEST_HASHING_ALGORITHM = HashAlgorithm.SHA2_256;
  // Sometimes manifest is too long despite the fact it is checked with
  // TLVElement.MAX_TLV16_CONTENT_LENGTH, so I added a few BYTES to prevent crashing
  private static final int FEW_ADDITIONAL_BYTES = 5;
  private ZipContainer zipContainer;

  Container(String containerUri) {
    zipContainer = new ZipContainer(new File(containerUri));
  }

  Container signFile(HashAlgorithm hashAlgorithm, String path) {
    return addFileAndSign(hashAlgorithm, new File(path));
  }

  Container signFilesWithFileUris(HashAlgorithm fileHashingAlgorithm, List<String> fileUris) {
    return signFiles(fileHashingAlgorithm,
        fileUris.stream().map(File::new).collect(Collectors.toList()));
  }

  Container signFiles(HashAlgorithm fileHashingAlgorithm, List<File> files) {
    try {
      var manifest = new Manifest(manifestId());

      zipContainer.add(new ArrayList<>(files));
      for (File file : files) {
        var datafileStructure = new ManifestElement(fileHashingAlgorithm, file);
        if (manifestIsTooLarge(manifest, datafileStructure)) {
          //close and write previous manifest
          writeManifest(manifest);
          // begin with new manifest after closing previous
          manifest = new Manifest(manifestId());
        }
        manifest.add(datafileStructure);
      }

      writeManifest(manifest);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return this;
  }

  Container removeSignature(String signatureUri) {
    var signedFileUris = filesSignedBy(signatureUri);
    var manifestUri = manifestUriOf(signatureUri);

    zipContainer.remove(signatureUri)
        .removeAll(signedFileUris)
        .remove(manifestUri);

    return this;
  }

  List<Manifest> signedManifests() {
    var signedManifests = new ArrayList<Manifest>();
    zipContainer.readFileByFile((bytes, filename) -> {
      if (isManifest(filename)) {
        signedManifests.add(createManifest(filename, bytes));
      }
    });
    return signedManifests;
  }

  List<String> signedFileUris() {
    return signedManifests()
        .stream()
        .flatMap(manifest -> manifest.signedFilesUris().stream())
        .collect(Collectors.toList());
  }

  private String manifestUriOf(String signatureUri) {
    return manifestOf(signatureUri).uri();
  }

  private Manifest manifestOf(String signatureUri) {
    Optional<Manifest> manifestOptional = signedManifests().stream()
        .filter(manifest -> manifest.signatureUri().equals(signatureUri))
        .findFirst();
    if (!manifestOptional.isPresent()) {
      throw new RuntimeException("No manifest found");
    }
    return manifestOptional.get();
  }

  private int manifestId() {
    return signedManifests().size() + 1;
  }

  private List<String> filesSignedBy(String signatureUri) {
    return signedManifests().stream()
        .filter(manifest -> manifest.signatureUri().equals(signatureUri))
        .flatMap(manifest -> manifest.signedFilesUris().stream())
        .collect(Collectors.toList());
  }

  private Container addFileAndSign(HashAlgorithm fileHashingAlgorithm, File containerFile) {
    return signFiles(fileHashingAlgorithm, asList(containerFile));
  }

  private boolean manifestIsTooLarge(Manifest manifest, ManifestElement datafileStructure) {
    return manifest.getContentLength() + datafileStructure.getContentLength() + FEW_ADDITIONAL_BYTES
        > TLVElement.MAX_TLV16_CONTENT_LENGTH;
  }

  private boolean isManifest(String uri) {
    return uri.startsWith("/META-INF/manifest");
  }

  private void writeManifest(Manifest manifest) {
    byte[] manifestBytes = toBytes(manifest);
    zipContainer.add("/META-INF/manifest" + manifestId() + ".tlv", manifestBytes);
    zipContainer.add(manifest.signatureUri(), KSI.hash(MANIFEST_HASHING_ALGORITHM, manifestBytes));
  }

  private byte[] toBytes(Manifest manifest) {
    try {
      var manifestBaos = new ByteArrayOutputStream();
      manifest.writeTo(manifestBaos);
      return manifestBaos.toByteArray();
    } catch (KSIException e) {
      throw new RuntimeException(e);
    }
  }

  private Manifest createManifest(String uri, byte[] bytes) {
    try {
      return new Manifest(uri, bytes);
    } catch (TLVParserException e) {
      throw new RuntimeException(e);
    }
  }
}
