1. Initially I wanted to use TLV parsing library and a ZIP library.
2. I used a zip library to get rid of tedious stuff (opening/closing of ZipStreams etc)
3. I did not used tlv library because there is a parser in a KSI library and a because of a
requirement to use build-in KSI TLV parser
4. Test coverage is about 90%, I did not checked the (rethrowing of) exceptions and 
other similar exception/io things.
5. I took almost 20h to complete - most difficult part: understanding and parsing of TLV. 
I was stumbled a few times with that.
6. Estonian Id container works like this exercise, so practically this exercise was interesting.